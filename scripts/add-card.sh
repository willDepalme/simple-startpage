#!/bin/bash

domain=$1
label=$2
echo "Domain : $domain"
echo "Label : $label"

echo "Fetching domain picture"
wget "https://s2.googleusercontent.com/s2/favicons?domain=$domain&sz=256" -O "../public/images/cards/$label.png" --quiet


book="\n{\n\tlabel: '$label',\n\turl: '$domain',\n\ticon: 'images/cards/$label.png'\n}"
entry=$(echo $book)

echo "Adding card entry to bookmarks.js"
sed -i "s|//CARD|,$entry//CARD|" ../public/bookmarks.js
