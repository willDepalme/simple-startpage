#!/bin/bash

domain=$1
label=$2
echo "Domain : $domain"
echo "Label : $label"

echo "Fetching domain icon"
wget "https://s2.googleusercontent.com/s2/favicons?domain=$domain&sz=32" -O "../public/images/$label.png" --quiet


book="\n{\n\tlabel: '$label',\n\turl: '$domain',\n\ticon: 'images/$label.png'\n}"
entry=$(echo $book)

echo "Adding entry to bookmarks.js"
sed -i "s|//BOOKMARK|,$entry//BOOKMARK|" ../public/bookmarks.js
