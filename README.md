# Simple Startpage

Simple startpage that let you search the web and access quickly websites.

See [Live Demo](https://willdepalme.gitlab.io/simple-startpage/).

![Home page with shortcut selected](public/images/screenshots/screen_1.png)
![Search in bookmarks](public/images/screenshots/screen_2.png)

![Demo Video](public/images/screenshots/demo.gif)

## Features

- Web search with DuckDuckGo
- Direct access to website when typing url
- Keyboard navigation to select bookmark
- Search in configured websites

## Configuration

First copy the bookmarks-exemple.js file to bookmarks.js.

### Autocomplete bookmarks

Automplete entries are configured in bookmarks.js file in the `bookmarks` array. A script is provided to add entries automatically.

Run `add-bookmarks.sh` with domain and label as parameter.

Commands:
```bash
chmod +x add-bookmarks.sh # Allow script execution
./add-bookmarks.sh https://gitlab.com Gitlab # pass domain with https and entry label
```

The script will download the domain favicon and save it under the folder `images` with the label provided as filename.
A new entry will be added in bookmarks.js. For the exemple, the new entry is :

```javascript
{
    label: "Github",
    url: "https://github.com",
    icon:"images/Github.png"
}//ADD
```

Note: You must keep the `//ADD` string in the file for the script to be able to add an entry

### Cards

A similar script is present to add new cards. The script has the same behavior except it download an image with higher resolution to be display in card.

Commands:
```bash
chmod +x add-card.sh # Allow script execution
./add-card.sh https://gitlab.com Gitlab # pass domain with https and entry label
```

A new entry will be added in bookmarks.js. For the exemple, the new entry is :

```javascript
{
    label: "Github",
    url: "https://github.com",
    icon:"images/Github.png"
}//CARD
```

Note: You must keep the `//CARD` string in the file for the script to be able to add an entry
