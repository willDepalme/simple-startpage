var currentFocus = -1;
var filteredOptions = [];
const regex = /.*\.(com|fr|io|dev)$/gm;

window.onload = function() {
    var searchInput = document.getElementById("search");
    var searchForm = document.getElementById("searchForm");
    var searchIcon = document.getElementById("search-icon");
    var datalist = document.getElementById("bookmarks");
    var nav = document.getElementsByTagName("nav")[0];
    var urlPattern = "https://duckduckgo.com/?q=";

    setTimeout(function() {
        searchIcon.classList.toggle("focus");
    }, 400);

    loadBookmarks(datalist, searchInput);
    loadCards(nav);

    searchForm.addEventListener("submit", function (event) {
        event.preventDefault();
        document.body.classList.add("loading");
        if (currentFocus == -1) {
            if (isUrl(searchInput.value)) {
                navigateToURL(searchInput.value, searchInput);
            } else {
                var searchUrl = urlPattern + encodeURI(searchInput.value);
                window.location = searchUrl;
            }
        } else {
            var pageOption = filteredOptions[currentFocus];
            datalist.classList.remove("open");
            currentFocus = -1;
            navigateToOptionPage(pageOption, searchInput);
        }
    }, true);

    searchInput.addEventListener("focus", function (event) {
        if (!searchIcon.classList.contains("focus")) {
            setTimeout(function() {
                searchIcon.classList.toggle("focus");
            }, 400);
        }
    });
    searchInput.addEventListener("blur", function (event) {
        if (searchIcon.classList.contains("focus")) {
            searchIcon.classList.remove("focus");
        }
        datalist.classList.remove("open");
    });
    
    searchInput.addEventListener("input", function(event){
        if(event.inputType == "insertText" || event.inputType.indexOf("delete") != -1  || event.inputType == null) {
            var options = datalist.options;
            var val = event.target.value.toLowerCase();
            filteredOptions = [];

            if (!val || !val.length) {
                console.log("RESET");
                resetDataList(datalist);
                return;
            } else if (!datalist.classList.contains("open")) {
                datalist.classList.add("open");
            }

            for (var i=0;i<options.length;i++){
                if (options[i].value.toLowerCase().indexOf(val) != -1 || options[i].textContent.toLowerCase().indexOf(val) != -1) {
                    options[i].style.display = "flex";
                    filteredOptions.push(options[i])
                } else {
                    options[i].style.display = "none";
                    options[i].classList.remove("active")
                }
            }
        }
    });

    searchInput.addEventListener("keydown", function(event) {
        if (event.code === "ArrowUp") {
            currentFocus--;
            checkFocusLimit();
            focusOption(datalist);
        } else if (event.code === "ArrowDown") {
            currentFocus++;
            checkFocusLimit();
            focusOption(datalist);
        } else if (event.code == "ArrowLeft") {
            document.body.classList.toggle("loading");
        }
    });

};

async function animateLinks(links) {
    for(var i = 0 ; i < links.length; i++) {
        var r = await new Promise(resolve => {
            setTimeout(() => {
                resolve();
            }, 75);
        });
        links[i].classList.add("open");
    }
}

function loadCards(nav) {
    var cardsDom = [];
    for (var i = 0 ; i < cards.length; i++) {
        var card = cards[i];
        var a = document.createElement("a");
        a.className = "link-card";
        a.href = card.url;

        var img = document.createElement("img");
        img.src = card.icon;
        a.appendChild(img);

        var label = document.createElement("span");
        label.innerText = card.label;
        a.appendChild(label);

        nav.appendChild(a);
        cardsDom.push(a);
    }

    animateLinks(cardsDom);
}

function loadBookmarks(datalist, searchInput) {
    for (var i = 0 ; i < bookmarks.length; i++) {
        var book = bookmarks[i];
        var opt = document.createElement("option");
        opt.value = book.url;
        opt.innerText = book.label;

        if (book.icon) {
            var icon = document.createElement("img");
            icon.src = book.icon;
            opt.appendChild(icon);
        }
        datalist.appendChild(opt);
    }


    for (var i = 0; i < datalist.options.length; i++) {
        datalist.options[i].addEventListener("mousedown", function(event) {
            navigateToOptionPage(event.target, searchInput);
        })
    }
}

/* ------- UTILS -------- */

function navigateToOptionPage(option, searchInput) {
    searchInput.value = ">  " + option.text;
    window.location = option.value;
}

function navigateToURL(url, searchInput) {
    searchInput.value = "> " + url;
    window.location = (url.indexOf("https://") == -1 ? "https://" : "") + url;
}

function checkFocusLimit() {
    if (currentFocus < 0) {
        currentFocus = 0;
    } else if (currentFocus >= filteredOptions.length) {
        currentFocus = filteredOptions.length - 1;
    }
}

function focusOption(datalist) {
    for (var i=0;i<filteredOptions.length;i++){
        if (i != currentFocus) {
            filteredOptions[i].classList.remove("active");
        } else {
            filteredOptions[i].classList.add("active");
            datalist.scrollTop = filteredOptions[i].offsetTop;
        }
    }
}


function resetDataList(datalist) {
    for (var i=0; i < datalist.options.length ; i++){
        datalist.options[i].classList.remove("active");
    }
    for (var i=0;i<filteredOptions.length;i++){
        filteredOptions[i].classList.remove("active");
    }   
    datalist.classList.remove("open");
    currentFocus = -1;
}

function isUrl(input) {
    return input.indexOf("https://") == 0 || input.match(regex) != null;
}
