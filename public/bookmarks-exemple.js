var bookmarks = [
    {
        label: "Youtube",
        url: "https://youtube.com",
        icon: "images/youtube.png"
    },
    {
        label: "Reddit",
        url: "https://reddit.com",
        icon: "images/reddit.ico"
    },
    {
        label: "Arch Wiki",
        url: "https://wiki.archlinux.org",
        icon: "images/arch-wiki.ico"
    },
    {
        label: "Gmail",
        url: "https://gmail.com/",
        icon: "images/gmail.ico"
    },
    {
        label: "Netflix",
        url: "https://netflix.com/",
        icon: "images/netflix.png"
    },
    {
        label: "Disney +",
        url: "https://disneyplus.com/",
        icon: "images/disney.svg"
    },
    {
        label: "Twitch",
        url: "https://www.twitch.tv/",
        icon: "images/twitch.ico"
    },
    {
        label: "Gitlab",
        url: "https://gitlab.com",
        icon: "images/Gitlab.png"
    },
    {
        label: "Github",
        url: "https://github.com",
        icon:"images/Github.png"
    }//BOOKMARK
];

var cards = [
    {
        label: "Youtube",
        url: "https://youtube.com",
        icon: "images/cards/youtube.png"
    },
    {
        label: "Netflix",
        url: "https://netflix.com/",
        icon: "images/cards/netflix.png"
    },
    {
        label: "Disney +",
        url: "https://disneyplus.com/",
        icon: "images/cards/disney.svg"
    },
    {
        label: "6Play",
        url: "https://6play.fr/",
        icon: "images/cards/6play.png"
    }//CARD
]